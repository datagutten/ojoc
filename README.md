# README #

OJOC (Open JOdel Client) is an *inofficial* proof-of-concept implementation of a Python Client for tellm/Jodel.

I have no affiliation with _The Jodel Venture GmbH_.
Get the official app in the PlayStore or AppStore.

## Ethics ##

* Don't be a jerk.

* Don't use OJOC or parts of it to spam or misbehave, or you'll ruin it for us all.

* Don't be a jerk.

## Prerequisites ##

* Python
* `python-gi`
* `python-requests`
* `python-appdirs`
* `python-shutil`
* `python-pillow`
* `python-enum34` to avoid "TypeError: 'type' object is not iterable" (Ty. Dominic S)
* `python-urwid`
* WebKit, WebKit-GTK (e.g., `gir1.2-webkit-3.0` under Debian)
* Notify (>=0.7) from gi-repository
* `petname` via `pip install petname`


Under Windows, install _PyGObject_ from https://sourceforge.net/projects/pygobjectwin32/

Under OSX, install _brew_ from 
http://brew.sh/

And then:

* `$ brew install python`
* `$ brew install gtk+3`
* `$ brew install adwaita-icon-theme`
* `$ brew install pygobject3`
* `$ pip install pillow`
* `$ pip install python-dateutil`
* `$ pip install keyring`
* `$ pip install python-enum`
* `$ pip install python-urwid`

For Emoji support, you need to install the Unicode Emoji symbols, e.g. via the _Symbola_ font.

* Under Debian, this is available via the package `ttf-ancient-fonts-symbola`
* Under Windows, get Symbola, e.g., from http://users.teilar.gr/~g1951d/Symbola.zip
* For OSX: no solution yet...

## OS Support ##

GUI Code successfully tested under

* Debian Testing, Python 2.7.11
* Windows 7 x64, Python 2.7.11
* OSX 10.10.5, With Python 2.7.11 from brew

Command-line tools also successfully ran under

* Raspbian Jessie

## Getting started ##

### Linux ###

Just run from any terminal:

`./ojoc.py`

### Windows ###

Just double-click on `ojoc.py` and hope for the best :stuck_out_tongue:

### OSX ###

Install Python via brew and all the packages mentioned above,
then run `python ojoc.py` in a terminal window.

## Captcha Verification ##

Since `4.32.1` or so, Captcha Verification is required to keep voting and posting.

This works as follows:

You get an URL of an image from the Jodel Server like this:

![img_5_6_8.png](https://bitbucket.org/repo/ebA9MM/images/180789487-img_5_6_8.png)

You have to identify in which of the sub images the Jodel racoons is shown, from left to right,
starting with 0.

In the above image, the images would be 5, 6, and 8.

All OJOC tools prompt the user on the command line if captcha verification is required.

You must then paste the URL into a browser, and enter the indices of the squares with the racoon
into the terminal separated by spaces.

## Command-line Stuff ##

Although `ojoc.py` is a shiny GUI app, it supports the following command line options:

    usage: ojoc.py [-h] [-c CITY,CC]

      -h, --help            show this help message and exit
      -c CITY,CC, --citycc CITY,CC
                            Your location, e.g. Vienna,AT

`nojoc.py` is a _Urwid_ based terminal friendly text app. Jodel over SSH!

    usage: ojoc.py [-h] [-c CITY,CC]

      -h, --help            show this help message and exit
      -c CITY,CC, --citycc CITY,CC
                            Your location, e.g. Vienna,AT

`ojoc_dumper.py` is a command-line tool to extract location and lexical dumps for
statistical analysis.

It can be called like this:

    At least one of --geodump or --lexical-analysis is required.
    usage: ojoc_dumper.py [-h] [-g PREFIX] [-l PREFIX] [-d DURATION] [-r DURATION]
                          [-c CITY,CC]

    optional arguments:
      -h, --help            show this help message and exit
      -g PREFIX, --geodump PREFIX
                            Dump geocoordinates to file <prefix>_<timestamp>.dump
      -l PREFIX, --lexical-analysis PREFIX
                            Dump lexical analysis to file
                            <prefix>_<timestamp>.dump
      -d DURATION, --dump-duration DURATION
                            Duration before saving in seconds
      -r DURATION, --refresh-duration DURATION
                            Duration before refreshing in seconds
      -c CITY,CC, --citycc CITY,CC
                            Your location, e.g. Vienna,AT

`ojoc_imgdump_fast.py` is a command-line tool for dumping all current images in the "recent" section.

It can be called like this:

    usage: ojoc_imgdump_fast.py [-h] [-r DURATION] [-l] [-o PATH] [-c CITY,CC]

    optional arguments:
      -h, --help            show this help message and exit
      -r DURATION, --refresh-duration DURATION
                            Duration before refreshing in seconds
      -l, --loop            Run in loop
      -o PATH, --output-dir PATH
                            Where to put the images
      -c CITY,CC, --citycc CITY,CC
                            Your location, e.g. Vienna,AT

`ojoc_imgdump.py` fulfills the same purpose, but keeps a state file and
tries to follow all seen posts with initial images.

## Screenshots ##

### OJOC ###

![OJOC](docs/ojoc.png)

### NOJOC ###

![NOJOC](docs/nojoc.png)

using [cool-retro-term](https://github.com/Swordfish90/cool-retro-term)

![NOJOC-CRT](docs/nojoc-crt.png)


## Hacky Hacker Stuff ##

The tellm Rest API is reachable via `https://api.go-tellm.com/api/v2/`.

It uses an SHA256 Hash over a Device ID as a user identifier (`device_uid`).

When OJOC is launched the first time, it generates a 256-bit random number from `os.urandom`
and uses this number as identifier to get an Access Token for *Bearer* Authorization.
The `device_uid` is stored in the OS' keyring using `keyring.set_password` under the
service *jodel* as *device_uid*

Once OJOC receives a valid Access Token, it stores it in `~/.jodel/jodel.cfg`

Before every request, OJOC checks if the Token is still valid, and requests a new one
if necessary.

### Key retrieval hack ###

The key retrieval hack has been moved to a separate repository:

https://bitbucket.org/cfib90/ojoc-keyhack

This repository is linked into the ojoc main repository as a git submodule.

## ToDo ##

* Keep up with HMAC Key changes
* Code Refactoring
* Adding additional features
* Keep up with API changes
